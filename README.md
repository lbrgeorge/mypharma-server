# MyPharma Server
Servidor para tratamento das imagens.

O primeiro passo para iniciar o servidor é instalar as dependências:
`npm install`

Depois é necessário configurar as credenciais do Watson no arquivo: `config.json`

Após isso, basta executar: 
`npm start`



# Config.json
`url` : URL da API do Watson.

`version` : Versão da API. (YYYY-MM-DD)

`iam_apikey` : Chave de para aplicação.
