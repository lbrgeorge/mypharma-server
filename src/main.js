const express = require('express');
const logger = require('./logger');
const Color = require('./constants').Color;

//Services
const recognition = require('./recognition');
const routes = require('./routes');

class Main {
    constructor() {
        this.app = express();

        /** Initialize Visual Recognition */
        recognition.init();
        /** Initialize Visual Recognition */

        /** Initialize express server */
        this.app.listen(8182, () => {
            routes.init(this.app);
            logger(`HTTP listening on port 8182`, Color.FgCyan);
        });
        /** Initialize express server */
    }
}

module.exports = new Main();