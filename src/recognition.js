const VisualRecognitionV3 = require('watson-developer-cloud/visual-recognition/v3');
const fs = require('fs');
const logger = require('./logger');
const Color = require('./constants').Color;
const config = require('../config.json');

class Recognition {
    init() {
        try {
            this.recognition = new VisualRecognitionV3(config.watson);
            logger(`Watson visual recognition initialized!`, Color.FgCyan);
        } catch (error) {
            logger(`Couldn't initialize watson!`, Color.FgRed);
            throw error;
        }
    }

    detectFaces(image) {
        return new Promise((resolve, reject) => {
            fs.exists(image, (b) => {
                if (b) {
                    this.recognition.detectFaces({images_file: fs.createReadStream(image)}, (err, res) => {
                        if (err) reject(err);
                        else {
                            if (res.images_processed > 0)
                            {
                                resolve(res.images[0].faces);
                            }
                            else reject("No image processed!");
                        }
                    })
                }
                else reject("Image file doesn't exist!");
            })
        })
    }
}

module.exports = new Recognition();