const fileUpload = require('express-fileupload');
const fs = require('fs');
const path = require('path');
const uuidv4 = require('uuid/v4');
const logger = require('./logger');
const Color = require('./constants').Color;

const recognition = require('./recognition');

class Routes {
    constructor() {
        this.app = undefined;

        this.send_result = {};
    }

    init(app) {
        this.app = app;

        this.app.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept POST GET PUT DELETE");
            res.header("Content-Type", "application/json");

            next();
        });

        app.use(fileUpload({
            limits: { fileSize: 5 * 1024 * 1024 },
        }));

        this.routes();
    }


    /** ROUTES */
    routes() {
        this.app.post('/upload', (req, res) => {
            if (req.files != undefined && req.files.image != undefined) {
                var image = req.files.image;

                if (image.mimetype == "image/jpg" || image.mimetype == "image/png" || image.mimetype == "image/jpeg") {
                    var filename = `${uuidv4()}${path.extname(image.name)}`;

                    image.mv(`${__dirname}/../data/${filename}`, (err) => {
                        if (err) this.sendResponse(res, {error: "Couldn't save file!"});
                        else {
                            recognition.detectFaces(`${__dirname}/../data/${filename}`).then((faces) => {
                                this.send_result = {image: filename, faces: faces};
                                this.sendResponse(res, this.send_result);
                            }, (err) => this.sendResponse(res, {error: err}));
                        }
                    })
                }
            }
            else this.sendResponse(res, {error: "No file submited!"});
        });

        this.app.get('/image/:file', (req, res) => {
            var filename = req.params.file;

            if (filename != undefined)
            {
                fs.exists(`${__dirname}/../data/${filename}`, (b) => {
                    if (b) {
                        var ext = path.extname(filename).replace('.', '');

                        res.header("Content-Type", "image/" + ext);
                        res.sendFile(path.resolve(`data/${filename}`));
                    }
                    else res.status(404).send('Not found');
                });
            }
        });
    }

    /** Send response as json */
    sendResponse(res, instant_result = undefined) {
        if (instant_result != undefined) this.send_result = instant_result;

        res.send(JSON.stringify(this.send_result));

        //Clear result
        this.send_result = {};
    }
}

module.exports = new Routes();